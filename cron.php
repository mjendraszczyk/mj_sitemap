<?php
/**
 * File to run a CRON job
 * @author MAGES Michał Jendraszczyk
 * @copyright (c) 2019, MAGES Michał Jendraszczyk
 * @license http://mages.pl MAGES Michał Jendraszczyk
 */

include_once '../../config/config.inc.php';
include_once 'mjpsxmlsitemap.php';

$updateSitemap = new Mjpsxmlsitemap();

if (Configuration::get('mj_sitemap_cron_token') == Tools::getValue('token')) {
    $updateSitemap->cronSitemap();

    echo "\nOK";
} else {
    echo "\nWrong token!";
}
